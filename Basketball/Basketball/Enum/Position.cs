﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Basketball.Enum
{
    public enum Position
    {
        PG,
        SG, 
        SF, 
        PF,
        C

    };
}
