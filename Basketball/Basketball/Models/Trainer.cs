﻿using Basketball.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Basketball
{
    public class Trainer : Person
    {
        public int YearsAsATrainer { get; set; }
    }
}
