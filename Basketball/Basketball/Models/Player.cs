﻿using Basketball.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Basketball.Models
{
    public class Player : Person
    {
       [ForeignKey ("Team")]
       public double Weight { get; set; }
       public int height { get; set; }
       [Required]
       public int JerseyNumber { get; set; }
       [Required]
       public Position Position { get; set; }
    }
}
