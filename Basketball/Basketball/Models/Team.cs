﻿using Basketball.Enum;
using Basketball.Interface;
using Basketball.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Basketball
{
    public class Team : IEntity<int>
    {

        public int ID { get; set; }
        public string Name { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public Conference Conference { get; set; }
        [Required]
        public DateTime YearOfreation { get; set; }
        [Required]
        public int SeasonWins { get; set; }
        public int lossesInTheSeason { get; set; }
        public Trainer Trainer { get; set; }
        [Required]
        public Player Player { get; set; }

    }
}
