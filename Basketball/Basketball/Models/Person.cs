﻿using Basketball.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Basketball.Models
{
    public class Person : IEntity<int>
    {
        [ForeignKey("Team")]
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public DateTime BirthDay { get; set; }
        

    }
}
