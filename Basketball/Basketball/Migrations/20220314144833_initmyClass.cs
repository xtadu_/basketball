﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Basketball.Migrations
{
    public partial class initmyClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Person",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Surname = table.Column<string>(nullable: false),
                    BirthDay = table.Column<DateTime>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    Weight = table.Column<double>(nullable: true),
                    height = table.Column<int>(nullable: true),
                    JerseyNumber = table.Column<int>(nullable: true),
                    Position = table.Column<int>(nullable: true),
                    YearsAsATrainer = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Team",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: false),
                    Conference = table.Column<int>(nullable: false),
                    YearOfreation = table.Column<DateTime>(nullable: false),
                    SeasonWins = table.Column<int>(nullable: false),
                    lossesInTheSeason = table.Column<int>(nullable: false),
                    TrainerID = table.Column<int>(nullable: true),
                    PlayerID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Team", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Team_Person_PlayerID",
                        column: x => x.PlayerID,
                        principalTable: "Person",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Team_Person_TrainerID",
                        column: x => x.TrainerID,
                        principalTable: "Person",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Person_JerseyNumber",
                table: "Person",
                column: "JerseyNumber",
                unique: true,
                filter: "[JerseyNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Team_Name",
                table: "Team",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Team_PlayerID",
                table: "Team",
                column: "PlayerID");

            migrationBuilder.CreateIndex(
                name: "IX_Team_TrainerID",
                table: "Team",
                column: "TrainerID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Team");

            migrationBuilder.DropTable(
                name: "Person");
        }
    }
}
