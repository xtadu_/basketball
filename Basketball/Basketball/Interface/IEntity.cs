﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Basketball.Interface
{
    public interface IEntity <T>
    {
        public T ID { get; set; }
    }
}
